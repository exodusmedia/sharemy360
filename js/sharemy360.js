/**
 * ShareMy360 JavaScript Edition
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */

// TODO: youtube embed

// TODO: Test for a tour not found
// TODO: audio
// TODO: minimap in top right
// TODO: arrows for up, down, left right
// TODO: autopan?
// TODO: ui buttons
// TODO: zooming
// TODO: popup tooltips for hotspots
// TODO: preview panos for switching to new segment
// TODO: show a little arrow to rotate for current pano position
// TODO: support non 360-degree pano (hit edge)
// TODO: mute button
// TODO: fullscreen
// TODO: top menu of segments
// TODO: top breadcrumbs

if (!window.sm360)
{
	window.sm360 = {
		TICK_DELAY: 20,
		SLIDE_SPEED_MIN: 1,
		SLIDE_SPEED_MAX: 10,
		SLIDE_SPEED_MULTIPLIER: 0.05,
		IMAGE_SERVER: 'http://assets.sharemy360.com/',
		TOUR_SERVER: '',
		mouse: {
			is_down: false,
			down: {
				x: 0,
				y: 0
			},
			x: 0,
			y: 0
		},
		tours: {},
		tour_datas: {},
		current_pano: null, // the currently active panorama being used
		tick_interval: false,
		css_loaded: false,
		debug: function(message){
			$('#debug').prepend(message + '\n');
		}
	};
}

sm360.tick = function(){
	current_pano = sm360.current_pano;
	if (sm360.mouse.is_down && current_pano)
	{
		$viewport = current_pano;

		tour_type = $viewport.attr('data-type');
		if (tour_type == 'Linear') { return; }

		viewport_width = $viewport.width();

		$panorama = $viewport.children('.panorama');

		$images = $panorama.children('.images');
		total_width = $images.width();

		// the image, to use for reference
		$image = $images.find('img').eq(0);
		image_width = $image.width();

		// move the panorama a certain amount
		distance = sm360.mouse.x - sm360.mouse.down.x;
		speed = Math.floor(Math.min(sm360.SLIDE_SPEED_MAX, Math.max(sm360.SLIDE_SPEED_MIN, Math.abs(sm360.SLIDE_SPEED_MULTIPLIER * distance))));

		amount = speed;
		if (distance < 0)
		{
			amount *= -1;
		}
		
		new_position = $panorama.position().left + amount;

		// once the panorama has gone past itself on the left, circle around
		if (new_position > (viewport_width * -1))
		{
			new_position -= image_width;
		// once the panorama has gone past itself on the right, circle around
		} else if (new_position < (-1 * (total_width - viewport_width))) {
			new_position += image_width;
		}


		$panorama.css('left', new_position);
	}
};

sm360.enhance_mouse_event = function(e)
{
	// for firefox, the offsetX and offsetY don't work!
	$viewport = sm360.current_pano;
	if ($viewport == null) { return e; }
	viewport_offset = $viewport.offset();
	e.offsetX = e.pageX - viewport_offset.left;
	e.offsetY = e.pageY - viewport_offset.top;
	return e;
}

sm360.is_numeric = function(n)
{
	return !isNaN(parseFloat(n)) && isFinite(n);
}

sm360.navlink_click = function(e)
{
	e.preventDefault();

	$this = $(this);
	$link = $(this).find('a');
	segment_string = $link.attr('href').replace('#', '');
	parts = sm360.parse_segment_string(segment_string);

	$viewport = $this.parents('.viewport');
	tour_key = $viewport.attr('data-tour');

	$this.empty();
		
	$this.parents('.viewport').sharemy360(tour_key, {
		segment: parts.segment + '.' + parts.photo
	});
}

/**
 * A tour's data has been fetched from the server
 * @param object data
 */
sm360.tour_loaded = function(data, options)
{
	tour_key = data.key;

	$tours = $('.sharemy360');
	for (tour_index in $tours)
	{
		if (!sm360.is_numeric(tour_index)) { continue; }
		$tour = $($tours[tour_index]);
		if ($tour.hasClass('data-loaded')) { continue; }
		if ($tour.attr('data-tour') !== tour_key) { continue; }

		$tour.removeClass('data-loading');
		$tour.addClass('data-loaded');

		segment_id = Number($tour.attr('data-segment'));
		photo_id = Number($tour.attr('data-photo'));

		sm360.tour_datas[tour_key] = data;

		$viewport = $tour;
		viewport_height = $viewport.height();

		// load the images for a tour
		segments = data.segments;
		segment = sm360.get_segment_from_segments(segment_id, segments);
		if (!segment)
		{
			segment_id = 0;
			segment = data.segments[0];
		}

		$tour.attr('data-type', segment.type);

		photos = segment.photos;
		photo = sm360.get_photo_from_photos(photo_id, photos);

		if (!photo)
		{
			photo_id = 0;
			photo = segment.photos[0];
		}

		$top_bar = $('<div class="ui top-bar" />');
		$top_bar.append('<p>This is example content to be displayed</p>');
		$bottom_bar = $('<div class="ui bottom-bar" />');
		$bottom_bar.append('<p><strong>ShareMy360:</strong> Click and drag to look around. To explore, click an icon.</p>');
		$tour.append($top_bar);
		$tour.append($bottom_bar);
		$tour.append('<div class="panorama" />');
		$tour.append('<div class="overlay unselectable" />');
		
		// video popup
		$youtube = $('<div class="youtube modal" />');
		$youtube_video = $('<div class="video" />');
		$youtube_close = $('<div class="close" />');
		$youtube_close.click(sm360.youtube_hotspot_close);
		$youtube.append($youtube_video);
		$youtube.append($youtube_close);
		$tour.append($youtube);
		$youtube.hide();

		// viewport height minus ui bar heights
		photo_small_height = viewport_height - $top_bar.outerHeight() - $bottom_bar.outerHeight();
		photo_large_height = photo_small_height * 2;
		photo_url = sm360.IMAGE_SERVER + '/image.php?k=' + photo.key + '&f=' + photo.filename + '&h=' + photo_large_height;

		// display the image

		$panorama = $tour.find('.panorama');
		$images = $('<div class="images" />');
		$wrapper = $('<div class="image">');
		$image = $('<img src="' + photo_url + '" alt="" />');
		$image.height(photo_small_height);
		$wrapper.append($image);
		$images.append($wrapper);
		$panorama.append($images);
		$image.bind('load', sm360.image_loaded);

		// add all the hotspots into the tour
		hotspots = photo.hotspots;
		for (hotspot_id in hotspots)
		{
			hotspot = hotspots[hotspot_id];

			$hotspot = $('<div class="hotspot" />');
			$hotspot.attr('title', hotspot.label);
			$hotspot.css({
				position: 'absolute',
				top: hotspot.yPercent + '%',
				left: hotspot.xPercent + '%',
				zIndex: 1000
			});
			$icon = $('<img />');
			$icon.live('load', sm360.hotspot_image_loaded);
			$icon.attr('src', hotspot.iconPath);
			$icon.attr('title', hotspot.label);

			$hotspot.addClass(hotspot.type.toLowerCase());

			// Website link, show a URL to the path
			switch (hotspot.type)
			{
			case 'Document':
			case 'Website':
				$a = $('<a />');
				$a.attr('href', hotspot.path);
				$a.attr('target', '_blank');
				$a.append($icon);
				$hotspot.append($a);
				break;

			// TODO: NavLink // goes to a segment/photo 'path': "1.0" (segment 1, photo 0)
			case 'NavLink':
				$a = $('<a />');
				$a.attr('href', '#' + hotspot.path);
				$a.append($icon);
				$hotspot.append($a);
				$a.click(sm360.navlink_click);
				break;

			// TODO: Video
			case 'Video':
				$a = $('<a />');
				$a.attr('href', '#');
				$a.attr('data-video', hotspot.path);
				$a.append($icon);
				$hotspot.append($a);
				$a.click(sm360.youtube_hotspot_click);
				break;

			// Otherwise just add the icon to the hotspot..
			default:
				$hotspot.append($icon);
			}

			$wrapper.append($hotspot);
			if (options.on_hotspot)
			{
				options.on_hotspot($hotspot);
			}
		}

		// only on panoramic pictures is it cloned and animatable
		if (segment.type !== 'Linear')
		{
			$image = $images.children('.image');
			for (x = 0; x < 2; x++)
			{
				cloned = $image.clone(true, true);
				cloned.children('img').off('load');
				$images.append(cloned);
			}
		}

		// handle the events
		$overlay = $tour.find('.overlay');
		$overlay.live('mousedown', function(e){
			e = sm360.enhance_mouse_event(e);
			e.preventDefault();
			sm360.current_pano = $(this).parent();
			sm360.mouse.is_down = true;
			mouse = down = sm360.mouse.down;
			down.x = e.offsetX;
			down.y = e.offsetY;
		});
		$overlay.live('mouseup', function(e){
			e = sm360.enhance_mouse_event(e);
			e.preventDefault();
			sm360.current_pano = $(this).parent();
			sm360.mouse.is_down = false;
			down = sm360.mouse.down;
			down.x = 0;
			down.y = 0;
		});
		$overlay.live('mousemove', function(e){
			e = sm360.enhance_mouse_event(e);
			e.preventDefault();
			sm360.current_pano = $(this).parent();
			mouse = sm360.mouse;
			mouse.x = e.offsetX;
			mouse.y = e.offsetY;
		});
		$overlay.live('mouseenter', function(e){
			e = sm360.enhance_mouse_event(e);
			e.preventDefault();
			mouse = sm360.mouse;
			mouse.is_down = false;
			mouse.x = e.offsetX;
			mouse.y = e.offsetY;
		});
		$overlay.live('mouseleave', function(e){
			e = sm360.enhance_mouse_event(e);
			sm360.current_pano = null;
			mouse = sm360.mouse;
			mouse.x = null;
			mouse.y = null;
			e.preventDefault();
		});
	}
}

sm360.youtube_hotspot_close = function(e)
{
	e.preventDefault();
	$this = $(this);

	$youtube = $this.parents('.youtube');
	$youtube.hide();

	$video = $youtube.find('.video');
	$video.html('');
}

sm360.youtube_hotspot_click = function(e)
{
	e.preventDefault();
	$this = $(this);

	$viewport = $this.parents('.viewport');

	$youtube = $viewport.find('.youtube');
	$youtube.show();

	youtube_code = $this.attr('data-video');
	
	$video = $youtube.find('.video');
	$video.html('<iframe width="560" height="315" src="http://www.youtube.com/embed/' + youtube_code + '" frameborder="0" allowfullscreen></iframe>');
}

sm360.center_hotspot = function($hotspot)
{
	if ($hotspot.hasClass('centered')) { return; }
	$hotspot.addClass('centered');
	width = $hotspot.outerWidth();
	height = $hotspot.outerHeight();

	left_nudge = -1 * Math.floor(width / 2);
	top_nudge = -1 * Math.floor(height / 2);

	$hotspot.css({
		marginLeft: left_nudge + 'px',
		marginTop: top_nudge + 'px'
	});
}

sm360.hotspot_image_loaded = function()
{
	// all hotspots need to be moved to be centered
	$hotspots = $(this).parents('.hotspot');
	sm360.center_hotspot($hotspot);
}

sm360.parse_segment_string = function(input)
{
	input = String(input);
	parts = input.split('.');
	return {
		segment: parts[0],
		photo: parts[1]
	};
}

sm360.image_loaded = function()
{
	// when the image has loaded, put panorama into starting position
	$image = $this = $(this);
	image_width = $image.width();
	image_height = $image.height();

	$container = $this.parents('.image');
	$container.width(image_width);
	$container.height(image_height);

	$images = $this.parents('.images');
	full_width = $images.width();

	$panorama = $this.parents('.panorama');
	$viewport = $panorama.parent('.viewport');
	$viewport.removeClass('loading');
	$viewport.addClass('loaded');
	viewport_width = $viewport.width();

	$youtube = $viewport.find('.youtube');
	$youtube.height(image_height);


	// only non-linear tours can be panoramic
	tour_type = $viewport.attr('data-type');
	if (tour_type == 'Linear')
	{
		if (image_width < viewport_width)
		{
			initial_left = Math.floor((viewport_width - image_width) / 2); 
			$panorama.css({
				left: initial_left
			});
		}
	} else {
		//initial_left = -1 * Math.floor((full_width - viewport_width) / 2); 
		initial_left = -1 * image_width;

		$panorama.css({
			left: initial_left
		});
	}
}

sm360.get_photo_from_photos = function(nid, photos)
{
	for (i in photos)
	{
		photo = photos[i];
		if (photo.id == nid) return photo;
	}
	return null;
}

sm360.get_segment_from_segments = function(nid, segments)
{
	for (i in segments)
	{
		segment = segments[i];
		if (segment.nid == nid) return segment;
	}
	return null;
}

sm360.load_tour = function(tour_id, element, options)
{
	// fetch the data for the tour
	// FIXME: make this come from a datasource
	if (sm360.tour_datas[tour_id])
	{
		return sm360.tour_loaded(sm360.tour_datas[tour_id], options);
	}

	url = sm360.TOUR_SERVER + '/tours/' + tour_id + '.json';
	return $.getJSON(url, function(data){
		sm360.tour_loaded(data, options);
	});
}

$.fn.sharemy360 = function(tour_id, options)
{
	// register the tour
	$this = $(this);
	$this.addClass('sharemy360');
	$this.addClass('viewport');
	$this.addClass('data-loading');
	$this.addClass('loading');
	$this.removeClass('data-loaded');
	$this.removeClass('loaded');
	$this.empty();

	options = $.extend({
		segment: null,
		on_hotspot: glow_loop,
		width: 700,
		height: 500
	}, options);

	// store the segment and photo information for the tour
	// break the segment string up
	segment_parts = sm360.parse_segment_string(options.segment);
	$this.attr('data-segment', segment_parts.segment);
	$this.attr('data-photo', segment_parts.photo);
	$this.attr('data-tour', tour_id);
	
	$this.width(options.width);
	$this.height(options.height);

	sm360.load_tour(tour_id, $this, options);

	if (!options) { options = {}; }

	// include some CSS
	if (!sm360.css_loaded)
	{
		$this.after('<link rel="stylesheet" href="sharemy360.css" />');
		sm360.css_loaded = true;
	}

	// start the rotator ticking
	if (!sm360.tick_interval)
	{
		sm360.tick_interval = setInterval(sm360.tick, sm360.TICK_DELAY);
	}
};

$.fx.step.glow = function(fx) {
	var amount = Math.max(Math.floor(Math.sin(fx.now * Math.PI) * 1000) / 1000, 0.3);
	$(fx.elem).css({ boxShadow: 'rgba(255, 255, 255, ' + amount + ') 0 0 30px'});
};

// endless loop of glowing?
function glow_loop(element)
{
	element.animate({ glow : 1 }, { duration: 7000, complete : function(){ glow_loop($(this)); } });
}
